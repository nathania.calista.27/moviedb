$("#searchbutton").click(function () {
  var title = $("#title").val();
  $.ajax({
    url: `https://www.omdbapi.com/?t=` + title + `&apikey=63418bde`,
    data: {
    },
    success: function (result) {
      $(".result").empty();
      html = "";
      console.log(result.Title);
      if (result.Title == undefined) {
        $(".result").attr("style", `border: none; padding:none; ` ) ; 
        html += `<div id="error">`+ result.Error + `</div>`;
        $(".result").append(html);
      } else {
        html += `<table>`;
        html += `<tr><th> Title </th><td>` + result.Title + `</td></tr>`;
        html += `<tr><th> Year </th><td>` + result.Year + `</td></tr>`;
        html += `<tr><th> Rated </th><td>` + result.Rated + `</td></tr>`;
        html += `<tr><th> Released </th><td>` + result.Released + `</td></tr>`;
        html += `<tr><th> Runtime </th><td>` + result.Runtime + `</td></tr>`;
        html += `</table><div id="bottom"><img id="likes" src="static/img/favorite_border-24px.svg">`;
        html += `<img id="bookmark" src="static/img/bookmark_border-24px.svg"></div>`;
        $(".result").append(html);
        $(".result").attr("style", `border: var(--secondary) solid 1px; padding:1rem;` ) ; 

      }

      $("#likes").click(function () {
        if ($("#likes").attr("src") == "static/img/favorite-24px.svg") {
          $("#likes").attr("src", "static/img/favorite_border-24px.svg")
        } else {
          $("#likes").attr("src", "static/img/favorite-24px.svg")
        }
      })

      $("#bookmark").click(function () {
        if ($("#bookmark").attr("src") == "static/img/bookmark-24px.svg") {
          $("#bookmark").attr("src", "static/img/bookmark_border-24px.svg")
        } else {
          $("#bookmark").attr("src", "static/img/bookmark-24px.svg")
        }
      })

    },
  });
})

const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');

function switchTheme(e) {
  if (e.target.checked) {
      document.documentElement.setAttribute('data-theme', 'dark');
      localStorage.setItem('theme', 'dark'); 
  }
  else {
      document.documentElement.setAttribute('data-theme', 'light');
      localStorage.setItem('theme', 'light'); 
  }    
}
toggleSwitch.addEventListener('change', switchTheme, false);